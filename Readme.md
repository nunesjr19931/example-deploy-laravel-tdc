## Exemplo de deploy automatizado

Esse projeto implementa um exemplo simples de deploy automatizado utilizando gitlab-ci.yml.

Para rodar o projeto localmente execute os comandos:

```shell script
git clone https://gitlab.com/nunesjr19931/example-deploy-laravel-tdc.git
cd example-deploy-laravel-tdc
docker-compose up -d
```


